# atelier2

# GéoQuizz
GéoQuizz est un petit jeu qui consiste à répondre à un quiz géographique selon plusieurs difficultés et thématiques. 
Il faut pouvoir situer des lieux sur une carte à partir de photos.
Le score dépendra de votre précision et de votre rapidité.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
