const path = require("path");
module.exports = {
    //Pour faire fonctionner les fichiers de style externe à Vue
    pluginOptions: {
        'style-resources-loader': {
            preProcessor: 'scss',
            patterns: [path.resolve(__dirname, "./src/styles/global.scss")]
        }
    },
    css: {
        extract: false
    }
}




