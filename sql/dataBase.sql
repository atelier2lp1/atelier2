USE geoQuizz;

DROP TABLE IF EXISTS level;
CREATE TABLE level
(
  id   int(11)       NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name varchar(128)  NOT NULL,
  dist double(11, 2) NOT NULL,
  nbphotos int(11) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS series;
CREATE TABLE series
(
  id   int(11)       NOT NULL PRIMARY KEY AUTO_INCREMENT,
  city varchar(128)  NOT NULL,
  lat  double(11, 7) NOT NULL,
  lng  double(11, 7) NOT NULL,
  zoom int(11) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS game;
CREATE TABLE game
(
  id       int(11)      NOT NULL PRIMARY KEY AUTO_INCREMENT,
  status   tinyint      NOT NULL DEFAULT 1,
  score    int(11)      NOT NULL DEFAULT 0,
  player   varchar(255) NOT NULL,
  idSeries int(11)      NOT NULL,
  idLevel int(11)       NOT NULL,
  FOREIGN KEY (idSeries) REFERENCES series (id) ON DELETE CASCADE,
  FOREIGN KEY (idLevel) REFERENCES level (id) ON DELETE CASCADE

) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS photo;
CREATE TABLE photo
(
  id          int(11)       NOT NULL PRIMARY KEY AUTO_INCREMENT,
  description varchar(128)  NOT NULL,
  lat         double(11, 7),
  lng         double(11, 7),
  url         varchar(255)  NOT NULL,
  idSeries    int(11),
  FOREIGN KEY (idSeries) REFERENCES series (id) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS user;
CREATE TABLE user
(
  id          varchar(255)       NOT NULL PRIMARY KEY,
  email varchar(128)  NOT NULL,
  password varchar(128)  NOT NULL,
  token varchar(128) NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

