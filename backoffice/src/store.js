import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        token: false,
    },
    mutations: {
        
        /** Methode setToken
         * setter for state.token
         * @param {*} state 
         * @param {*} token 
         */
        setToken(state, token){
            state.token = token;
        },

        /** Methode initialiseStore
         * Initialize localStorage
         * @param {*} state 
         */
        initialiseStore(state) {
            if(localStorage.getItem('store')) {
                this.replaceState(
                    Object.assign(state, JSON.parse(localStorage.getItem('store')))
                );
            }
        }

    },
    actions: {

    }
})
