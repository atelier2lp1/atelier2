<?php


namespace geoQuizz\player\response;

use Slim\App;
use Slim\Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class Writter
{

    public static function jsonError($resp, $message, $code = 404) {
        $body = [
            "type" => "error",
            "error" => $code,
            "message" => $message
        ];

        $resp = $resp->withStatus($code)->withHeader('Content-Type', 'application/json;charset=utf-8');
        $respJson = json_encode($body);
        $resp->write("$respJson");
        return $resp;
    }

    public static function jsonSuccess($response, $result, $code = 200, $type = 'resource') {
        $result["locale"] = "fr-FR";
        $result["type"] = $type;
        $result = json_encode($result);
        $response->write("$result");

        return $response->withStatus($code)->withHeader('Content-Type', 'application/json;charset=utf-8') ;
    }

}