<?php

namespace geoQuizz\player\models;
use Illuminate\Database\Eloquent\Model;

class Series extends Model{

    protected $table = 'series';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function photos(){
        return $this->hasMany(
            'geoQuizz\player\models\Photo',
            'idSeries',
            'id'
        );
    }

    public function games(){
        return $this->hasMany(
            'geoQuizz\player\models\Game',
            'idSeries',
            'id'
        );
    }

}