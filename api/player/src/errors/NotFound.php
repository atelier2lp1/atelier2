<?php
/**
 * Created by PhpStorm.
 * User: jalil
 * Date: 12/18/18
 * Time: 5:06 PM
 */
namespace geoQuizz\player\errors;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use geoQuizz\player\response\Writter;

class NotFound{

    public static function error(Request $rq, Response $rs){
        $uri = $rq->getUri();
        return Writter::jsonError($rs, "$uri Not Found");

    }

}