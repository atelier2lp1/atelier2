<?php

use geoQuizz\player\controllers\GameController;
use geoQuizz\player\eloquent\Eloquent;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Container;
use geoQuizz\player\controllers\SeriesController;
use geoQuizz\player\controllers\LevelController;
use Slim\Exception\MethodNotAllowedException;
use Slim\Exception\NotFoundException;

require '../src/vendor/autoload.php';

$apiContainer = new Container(require_once __DIR__ . "/../src/conf/config.php");
$app = new App($apiContainer);

Eloquent::startEloquent($apiContainer->settings['dbconf']);

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

/**
 * @api {get} /series/{id}/photos[/]  Recuperer les photos d'une serie
 * @apiGroup Series
 * @apiName getPhotos
 * @apiVersion 1.0.0
 * @apiDescription Accès à une collection de type photos:
 * permet d'accéder à la représentation de la collection des photos désignée.
 * Retourne une représentation json de la collection, incluant sa latitude, sa longitude,
 * sa description, son url et l'id de la serie.
 * @apiParam {Number} id Identifiant unique de la serie
 * @apiSuccess (Succès : 200) {String} count nombre des photos par collection
 * @apisuccess (Succès : 200) {Object} photos Ressource photo retournée
 * @apiSuccess (Succès : 200) {Number} photo.id Identifiant de la photo
 * @apiSuccess (Succès : 200) {String} photo.description Description de la photo
 * @apiSuccess (Succès : 200) {Number} photo.lat Latitude de la photo
 * @apiSuccess (Succès : 200) {Number} photo.lng Longitude de la photo
 * @apiSuccess (Succès : 200) {String} photo.url Url de la photo
 * @apiSuccess (Succès : 200) {Number} photo.idSeries Id de la serie correspondant
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici collection
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "count": 1,
 *        "photos": [
 *          {
 *              "id": 13,
 *              "description": "Nancy Museum-Aquarium",
 *              "lat": 48.6948927,
 *              "lng": 6.1881627,
 *              "url": "https://res.cloudinary.com/geoquizz/image/upload/v1552572777/aquarium.jpg",
 *              "idSeries": 1
 *          }
 *        "locale": "fr-FR",
 *        "type": "collection"
 *     }
 * @apiError (Erreur : 404) NotFound Serie inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->get('/series/{id}/photos[/]',
    function (Request $request, Response $response, array $args) {
        return (new SeriesController($this))->getPhotos($request, $response, $args);
    }
);


/**
 * @api {post} /games[/]  Créer un jeu
 * @apiGroup Games
 * @apiName createGame
 * @apiVersion 1.0.0
 * @apiDescription
 * Permet de créer un jeu avec un status, score, le nom du joueur, l'id de la serie et
 * l'id du niveau.
 * @apiParam {Number} status Status du jeu, 1 = en train de jouer, 0 = pause, 2 = fini
 * @apiParam {String} player Nom du joueur
 * @apiParam {Number} idSeries L'id de la serie correspondant
 * @apiParam {Number} idLevel L'id du niveau correspondant
 * @apisuccess (Succès : 200) {Object} Game Ressource jeu retournée
 * @apiSuccess (Succès : 200) {Number} game.id Identifiant du jeu
 * @apiSuccess (Succès : 200) {Number} game.status Status du jeu
 * @apiSuccess (Succès : 200) {Number} game.score Score de la photo
 * @apiSuccess (Succès : 200) {String} game.player Longitude de la photo
 * @apiSuccess (Succès : 200) {Number} photo.idSeries Id de la serie correspondant
 * @apiSuccess (Succès : 200) {Number} photo.idLevel  Id du niveau correspondant
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici ressource
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "game":
 *          {
 *              "id": 1,
 *              "status": 2,
 *              "score": 581,
 *              "player": "Jalil",
 *              "idSeries": 1,
 *              "idLevel": 1
 *          }
 *        "locale": "fr-FR",
 *        "type": "resource"
 *     }
 * @apiError (Erreur : 404) NotFound Game inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->post('/games[/]',
    function (Request $request, Response $response, array $args) {
        return (new GameController($this))->create($request, $response, $args);
    }
);

/**
 * @api {get} /games/[{id}]  Recuperer un jeu
 * @apiGroup Games
 * @apiName getGame
 * @apiVersion 1.0.0
 * @apiDescription
 * Permet d'afficher un jeu avec un status, score, le nom du joueur, l'id de la serie et
 * l'id du niveau. Ou afficher les meilleures jeux avec l'id 'best'
 * @apiParam {Number} id Id du jeu
 * @apisuccess (Succès : 200) {Object} Game Ressource jeu retournée
 * @apiSuccess (Succès : 200) {Number} game.id Identifiant du jeu
 * @apiSuccess (Succès : 200) {Number} game.status Status du jeu
 * @apiSuccess (Succès : 200) {Number} game.score Score de la photo
 * @apiSuccess (Succès : 200) {String} game.player Longitude de la photo
 * @apiSuccess (Succès : 200) {Number} photo.idSeries Id de la serie correspondant
 * @apiSuccess (Succès : 200) {Number} photo.idLevel  Id du niveau correspondant
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici ressource
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "game":
 *          {
 *              "id": 1,
 *              "status": 2,
 *              "score": 581,
 *              "player": "Jalil",
 *              "idSeries": 1,
 *              "idLevel": 1
 *          }
 *        "locale": "fr-FR",
 *        "type": "resource"
 *     }
 * @apiError (Erreur : 404) NotFound Game inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->get('/games/[{id}]',
    function (Request $request, Response $response, array $args) {
        return (new GameController($this))->index($request, $response, $args);
    }
);

/**
 * @api {get} /levels/[{id}] Recuperer un niveau
 * @apiGroup Level
 * @apiName getLevel
 * @apiVersion 1.0.0
 * @apiDescription
 * Permet d'afficher un niveau avec la distance maximale, et le nombre des photos
 * @apiParam {Number} id L'id du niveau
 * @apisuccess (Succès : 200) {Object} Level Ressource niveau retournée
 * @apiSuccess (Succès : 200) {Number} Level.id Identifiant du niveau
 * @apiSuccess (Succès : 200) {String} Level.name Nom du niveau
 * @apiSuccess (Succès : 200) {Number} Level.dist Distance maximale pour avoir des points
 * @apiSuccess (Succès : 200) {Number} Level.nbphotos Nombre des photos par niveau
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici ressource
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "Level":
 *          {
 *              "id": 3,
 *              "name": "Difficile",
 *              "dist": 500,
 *              "nbphotos": 10,
 *          }
 *        "locale": "fr-FR",
 *        "type": "resource"
 *     }
 * @apiError (Erreur : 404) NotFound Level inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->get('/levels/[{id}]',
    function (Request $request, Response $response, array $args) {
        return (new LevelController($this))->getLevels($request, $response, $args);

    }
);

/**
 * @api {get} /series/[{id}] Recuperer une serie
 * @apiGroup Series
 * @apiName getSeries
 * @apiVersion 1.0.0
 * @apiDescription
 * Permet d'afficher une serie avec la ville ou elle est, sa latitude, sa longitude
 * et le zoom de la ville.
 * @apiParam {Number} id L'id de la serie
 * @apisuccess (Succès : 200) {Object} Series Ressource Series retournée
 * @apiSuccess (Succès : 200) {Number} Series.id Identifiant de la serie
 * @apiSuccess (Succès : 200) {String} Series.city Le nom de la ville
 * @apiSuccess (Succès : 200) {Number} Series.lat Latitude de la ville
 * @apiSuccess (Succès : 200) {Number} Series.lng Longitude de la ville
 * @apiSuccess (Succès : 200) {Number} Series.zoom Zoom de la ville
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici ressource
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "Series":
 *          {
 *              "id": 1,
 *              "city": "Nancy",
 *              "lat": 48.6876972,
 *              "lng": 6.1843538,
 *              "zoom": 18,
 *          }
 *        "locale": "fr-FR",
 *        "type": "resource"
 *     }
 * @apiError (Erreur : 404) NotFound Serie inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->get('/series/[{id}]',
    function (Request $request, Response $response, array $args) {
        //if (isset($args['id']))
        return (new SeriesController($this))->getSeries($request, $response, $args);
        //else
        //    return (new SeriesController($this))->getAllSeries($request, $response, $args);
    }
);

/**
 * @api {patch} /games/{id} Modifie un jeu existant
 * @apiGroup Games
 * @apiName updateGame
 * @apiVersion 1.0.0
 * @apiDescription
 * Permet de modifier un jeu avec le status et/ou le score
 * @apiParam {Number} id Id du jeu
 * @apiParam {Number} score Permet modifier le score du jeu
 * @apiParam {Number} status Permet modifier le status du jeu
 * @apisuccess (Succès : 200) {Object} Game Ressource jeu retournée
 * @apiSuccess (Succès : 200) {Number} game.id Identifiant du jeu
 * @apiSuccess (Succès : 200) {Number} game.status Status du jeu
 * @apiSuccess (Succès : 200) {Number} game.score Score de la photo
 * @apiSuccess (Succès : 200) {String} game.player Longitude de la photo
 * @apiSuccess (Succès : 200) {Number} photo.idSeries Id de la serie correspondant
 * @apiSuccess (Succès : 200) {Number} photo.idLevel  Id du niveau correspondant
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici ressource
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "game":
 *          {
 *              "id": 1,
 *              "status": 2,
 *              "score": 581,
 *              "player": "Jalil",
 *              "idSeries": 1,
 *              "idLevel": 1
 *          }
 *        "locale": "fr-FR",
 *        "type": "resource"
 *     }
 * @apiError (Erreur : 404) NotFound Game inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->patch('/games/{id}',
    function (Request $request, Response $response, array $args) {
        return (new GameController($this))->update($request, $response, $args);
    }
);


$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function ($req, $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});

$app->run();