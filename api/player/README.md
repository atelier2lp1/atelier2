###API PLAYER http://api.player.local:10080

####Description :
L'API du joueur permet de gérer les jeux (ajouter, modifier, afficher), ainsi que les photos de chaque jeu en fonction de leur difficulté et de leur ville. L'utilisateur peut jouer grâce à une carte et des points avec les coordonnées GPS en fonction des points de l'image

| Route         	    | Method     | Header Params  			    | Body Params 									    | Description 				                 |
| ----------------------|:----------:| ----------------------------:|--------------------------------------------------:|-------------------------------------------:|
| /series/{id}/photos[/]| GET        | none                         | none    										    | Affiche toutes les photos du jeu selon la serie et la difficulté|
| /series[{id}]    	    | GET        | none                         | none    										    | Affiche une une serie s'il y a un id, sinon toutes les series|
| /games[{id}]     	    | GET        | none                         | none    										    | Affiche une partie ou les meilleurs (avec id 'best')|
| /levels[{id}]     	| GET        | none                         | none                    							| Affiche un niveau                          |
| /games[/]    	        | POST 	     | none                         | status,player,idSeries,idLevel                	| Crée une serie                             |
| /games/{id}[/]        | PATCH      | none                         | score et/ou status  							    | Modifie la serie selon l'attribute         |
