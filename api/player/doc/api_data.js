define({ "api": [
  {
    "type": "post",
    "url": "/games[/]",
    "title": "Créer un jeu",
    "group": "Games",
    "name": "createGame",
    "version": "1.0.0",
    "description": "<p>Permet de créer un jeu avec un status, score, le nom du joueur, l'id de la serie et l'id du niveau.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Status du jeu, 1 = en train de jouer, 0 = pause, 2 = fini</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "player",
            "description": "<p>Nom du joueur</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "idSeries",
            "description": "<p>L'id de la serie correspondant</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "idLevel",
            "description": "<p>L'id du niveau correspondant</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "Object",
            "optional": false,
            "field": "Game",
            "description": "<p>Ressource jeu retournée</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "game.id",
            "description": "<p>Identifiant du jeu</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "game.status",
            "description": "<p>Status du jeu</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "game.score",
            "description": "<p>Score de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "game.player",
            "description": "<p>Longitude de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.idSeries",
            "description": "<p>Id de la serie correspondant</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.idLevel",
            "description": "<p>Id du niveau correspondant</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici ressource</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Langage de la réponse, ici fr-FR</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 200 OK\n{\n   \"game\":\n     {\n         \"id\": 1,\n         \"status\": 2,\n         \"score\": 581,\n         \"player\": \"Jalil\",\n         \"idSeries\": 1,\n         \"idLevel\": 1\n     }\n   \"locale\": \"fr-FR\",\n   \"type\": \"resource\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Game inexistante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 404",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"type\" : \"error',\n  \"error\" : 404,\n  \"message\" : Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api_player/index.php",
    "groupTitle": "Games"
  },
  {
    "type": "get",
    "url": "/games/[{id}]",
    "title": "Recuperer un jeu",
    "group": "Games",
    "name": "getGame",
    "version": "1.0.0",
    "description": "<p>Permet d'afficher un jeu avec un status, score, le nom du joueur, l'id de la serie et l'id du niveau. Ou afficher les meilleures jeux avec l'id 'best'</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Id du jeu</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "Object",
            "optional": false,
            "field": "Game",
            "description": "<p>Ressource jeu retournée</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "game.id",
            "description": "<p>Identifiant du jeu</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "game.status",
            "description": "<p>Status du jeu</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "game.score",
            "description": "<p>Score de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "game.player",
            "description": "<p>Longitude de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.idSeries",
            "description": "<p>Id de la serie correspondant</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.idLevel",
            "description": "<p>Id du niveau correspondant</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici ressource</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Langage de la réponse, ici fr-FR</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 200 OK\n{\n   \"game\":\n     {\n         \"id\": 1,\n         \"status\": 2,\n         \"score\": 581,\n         \"player\": \"Jalil\",\n         \"idSeries\": 1,\n         \"idLevel\": 1\n     }\n   \"locale\": \"fr-FR\",\n   \"type\": \"resource\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Game inexistante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 404",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"type\" : \"error',\n  \"error\" : 404,\n  \"message\" : Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api_player/index.php",
    "groupTitle": "Games"
  },
  {
    "type": "patch",
    "url": "/games/{id}",
    "title": "Modifie un jeu existant",
    "group": "Games",
    "name": "updateGame",
    "version": "1.0.0",
    "description": "<p>Permet de modifier un jeu avec le status et/ou le score</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Id du jeu</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "score",
            "description": "<p>Permet modifier le score du jeu</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Permet modifier le status du jeu</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "Object",
            "optional": false,
            "field": "Game",
            "description": "<p>Ressource jeu retournée</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "game.id",
            "description": "<p>Identifiant du jeu</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "game.status",
            "description": "<p>Status du jeu</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "game.score",
            "description": "<p>Score de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "game.player",
            "description": "<p>Longitude de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.idSeries",
            "description": "<p>Id de la serie correspondant</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.idLevel",
            "description": "<p>Id du niveau correspondant</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici ressource</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Langage de la réponse, ici fr-FR</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 200 OK\n{\n   \"game\":\n     {\n         \"id\": 1,\n         \"status\": 2,\n         \"score\": 581,\n         \"player\": \"Jalil\",\n         \"idSeries\": 1,\n         \"idLevel\": 1\n     }\n   \"locale\": \"fr-FR\",\n   \"type\": \"resource\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Game inexistante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 404",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"type\" : \"error',\n  \"error\" : 404,\n  \"message\" : Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api_player/index.php",
    "groupTitle": "Games"
  },
  {
    "type": "get",
    "url": "/levels/[{id}]",
    "title": "Recuperer un niveau",
    "group": "Level",
    "name": "getLevel",
    "version": "1.0.0",
    "description": "<p>Permet d'afficher un niveau avec la distance maximale, et le nombre des photos</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>L'id du niveau</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "Object",
            "optional": false,
            "field": "Level",
            "description": "<p>Ressource niveau retournée</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Level.id",
            "description": "<p>Identifiant du niveau</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "Level.name",
            "description": "<p>Nom du niveau</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Level.dist",
            "description": "<p>Distance maximale pour avoir des points</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Level.nbphotos",
            "description": "<p>Nombre des photos par niveau</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici ressource</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Langage de la réponse, ici fr-FR</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 200 OK\n{\n   \"Level\":\n     {\n         \"id\": 3,\n         \"name\": \"Difficile\",\n         \"dist\": 500,\n         \"nbphotos\": 10,\n     }\n   \"locale\": \"fr-FR\",\n   \"type\": \"resource\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Level inexistante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 404",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"type\" : \"error',\n  \"error\" : 404,\n  \"message\" : Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api_player/index.php",
    "groupTitle": "Level"
  },
  {
    "type": "get",
    "url": "/series/{id}/photos[/]",
    "title": "Recuperer les photos d'une serie",
    "group": "Series",
    "name": "getPhotos",
    "version": "1.0.0",
    "description": "<p>Accès à une collection de type photos: permet d'accéder à la représentation de la collection des photos désignée. Retourne une représentation json de la collection, incluant sa latitude, sa longitude, sa description, son url et l'id de la serie.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Identifiant unique de la serie</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "count",
            "description": "<p>nombre des photos par collection</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Object",
            "optional": false,
            "field": "photos",
            "description": "<p>Ressource photo retournée</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.id",
            "description": "<p>Identifiant de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "photo.description",
            "description": "<p>Description de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.lat",
            "description": "<p>Latitude de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.lng",
            "description": "<p>Longitude de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "photo.url",
            "description": "<p>Url de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.idSeries",
            "description": "<p>Id de la serie correspondant</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici collection</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Langage de la réponse, ici fr-FR</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 200 OK\n{\n   \"count\": 1,\n   \"photos\": [\n     {\n         \"id\": 13,\n         \"description\": \"Nancy Museum-Aquarium\",\n         \"lat\": 48.6948927,\n         \"lng\": 6.1881627,\n         \"url\": \"https://res.cloudinary.com/geoquizz/image/upload/v1552572777/aquarium.jpg\",\n         \"idSeries\": 1\n     }\n   \"locale\": \"fr-FR\",\n   \"type\": \"collection\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Serie inexistante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 404",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"type\" : \"error',\n  \"error\" : 404,\n  \"message\" : Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api_player/index.php",
    "groupTitle": "Series"
  },
  {
    "type": "get",
    "url": "/series/[{id}]",
    "title": "Recuperer une serie",
    "group": "Series",
    "name": "getSeries",
    "version": "1.0.0",
    "description": "<p>Permet d'afficher une serie avec la ville ou elle est, sa latitude, sa longitude et le zoom de la ville.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>L'id de la serie</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "Object",
            "optional": false,
            "field": "Series",
            "description": "<p>Ressource Series retournée</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Series.id",
            "description": "<p>Identifiant de la serie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "Series.city",
            "description": "<p>Le nom de la ville</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Series.lat",
            "description": "<p>Latitude de la ville</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Series.lng",
            "description": "<p>Longitude de la ville</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Series.zoom",
            "description": "<p>Zoom de la ville</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici ressource</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Langage de la réponse, ici fr-FR</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 200 OK\n{\n   \"Series\":\n     {\n         \"id\": 1,\n         \"city\": \"Nancy\",\n         \"lat\": 48.6876972,\n         \"lng\": 6.1843538,\n         \"zoom\": 18,\n     }\n   \"locale\": \"fr-FR\",\n   \"type\": \"resource\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Serie inexistante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 404",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"type\" : \"error',\n  \"error\" : 404,\n  \"message\" : Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api_player/index.php",
    "groupTitle": "Series"
  }
] });
