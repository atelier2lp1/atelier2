###API BACKOFFICE http://api.backoffice.local:30080

####Description :
L'api backoffice permet de gérer les séries (liste/ajout/modification), ainsi que les photos (liste/ajout/modification) et les niveaux (liste/ajout). L'utilisateur doit se connecter pour pouvoir accès aux fonctionnalités précédement citées. (Il peut biensur de créer un compte)

| Route         	    | Method     | Header Params  			    | Body Params 									    | Description 				                 |
| ----------------------|:----------:| ----------------------------:|--------------------------------------------------:|-------------------------------------------:|
| /series[/]    	    | GET        | Authorization Bearer {token} | none    										    | Affiche une partie des series (paginaton)  |
| /photos[/]    	    | GET        | Authorization Bearer {token} | none    										    | Affiche une partie des photos (pagination) |
| /levels[/]     	    | GET        | Authorization Bearer {token} | name, dist, nbphotos    							| Affiche une partie des levels (pagination) |
| /series[/]    	    | POST 	     | Authorization Bearer {token} | city,lat,lng,photos(array idPhotos, min:10)    	| Crée une serie                             |
| /photos[/]    	    | POST       | Authorization Bearer {token} | description, lat(nullable), lng(nullable), url    | Crée une photos 			                 | 
| /users[/]     	    | POST       | Authorization Bearer {token} | none    										    | Crée un utilisateur 		                 |
| /users/login[/]     	| POST       | Authorization Basic          | none    										    | Connecte un utilisateur 		             |
| /levels[/]     	    | POST       | Authorization Bearer {token} | name, dist, nbphotos    						    | Ajoute un nouveau niveau 		             |
| /series/{id}[/]       | PATCH      | Authorization Bearer {token} | city,lat,lng,dist    							    | Modifie la serie			                 |
| /photos/{id}[/]       | PATCH      | Authorization Bearer {token} | lat,lng,series (id)   						    | Modifie la photo 			                 |
| /users[/]     	    | DELETE     | Authorization Bearer {token} | none    										    | Deconnecte un utlisateur 		             |