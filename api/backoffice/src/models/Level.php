<?php

namespace geoQuizz\backoffice\models;
use Illuminate\Database\Eloquent\Model;

class Level extends Model{

    protected $table = 'level';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function games(){
        return $this->hasMany(
            'geoQuizz\backoffice\models\Game',
            'idLevel',
            'id'
        );
    }

}