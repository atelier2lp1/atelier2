<?php
/**
 * Created by PhpStorm.
 * User: jalil
 * Date: 12/18/18
 * Time: 5:13 PM
 */
namespace geoQuizz\backoffice\errors;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class NotAllowed{

    public static function error(Request $rq, Response $rs, $methods){
            $method = $rq->getMethod();
            $uri = $rq->getUri();
            $result['type'] = "error";
            $result['error'] = 405;
            $result['msg'] = "Method not allowed $method in $uri";
            $resp = $rs
                ->withHeader('Allow',$methods)
                ->withStatus(405);
            $resp->getBody()->write(json_encode($result));
            return $resp;
    }

}