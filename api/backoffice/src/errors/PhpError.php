<?php
/**
 * Created by PhpStorm.
 * User: jalil
 * Date: 12/18/18
 * Time: 5:17 PM
 */
namespace geoQuizz\backoffice\errors;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use geoQuizz\backoffice\response\Writter;

class PhpError{

    public static function error(Request $rq, Response $rs, $error){
            return Writter::jsonError($rs, $error, 500);

    }

}