<?php

namespace geoQuizz\backoffice\controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use geoQuizz\backoffice\response\Writter;
/* Errors */
use geoQuizz\backoffice\errors\NotAllowed;
use geoQuizz\backoffice\errors\NotFound;
use geoQuizz\backoffice\errors\PhpError;
/* Models */
use geoQuizz\backoffice\models\Photo;
use geoQuizz\backoffice\models\Series;

class PhotoController{

    protected $app;

    public function __construct($pApp){
        $this->app = $pApp;
    }

    /** Méthode get
     * Get a part of photos (pagination)
     * Params : page
     * 
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Array photos list
      */
    public function get(Request $request, Response $response, array $args){
        $limit = 5;
        if(!empty($request->getQueryParam('page'))){
            $page = $request->getQueryParam('page');
        }
        else{
            $page = 1;
        }
        $offset = (--$page) * $limit;
        $count = Photo::all()->count();
        $lastPage = ceil($count / $limit);
        $listPhotos = Photo::skip($offset)->take($limit)->get();

        $next = $page + 2;
        if($next > $lastPage){
            $next = $lastPage;
        }

        $prev = $page;
        if($prev == 0){
           $prev = 1; 
        }

        $data = [
            'count' => count($listPhotos->toArray()),
            'photos' => $listPhotos,
            'page' => [
                'prev' => $prev,
                'next' => $next,
                'first' => 1,
                'last' => $lastPage
            ]
        ];

        return Writter::jsonSuccess($response, $data, 206, 'collection');
    }


    /** Méthode create
     * Create a new photo
     * Body : description, lat (optionnal), lng (optionnal), url, series (optionnal)
     * (just share id of the series)
     * 
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
      */
    public function create(Request $request, Response $response, array $args): Response{
        $body = $request->getParsedBody();
        $error = false;
        $lat = null;
        $lng = null;

        if(isset($body['description'])){
            $description = filter_var($body['description'], FILTER_SANITIZE_STRING);
        }
        else{
            $error = true;
        }

        if(isset($body['lat'])){
            $lat = filter_var($body['lat']);
        }

        if(isset($body['lng'])){
            $lng = filter_var($body['lng']);
        }

        if(isset($body['url'])){
            $url = filter_var($body['url'], FILTER_SANITIZE_URL);
        }
        else{
            $error = true;
        }

        if(!$error){
            try{
                $photo = new Photo();
                $photo->description = $description;
                $photo->lat = $lat;
                $photo->lng = $lng;
                $photo->url = $url;

                if(isset($body['series'])){
                    $idSeries = filter_var($body['series'], FILTER_SANITIZE_NUMBER_INT);
                    $series = Series::find($idSeries);
                    $photo->series()->associate($series);
                }

                $photo->save();

                return Writter::jsonSuccess($response, array('photo' => $photo), 201);
            }
            catch(\Exception $e){
                return PhpError::error($request, $response, $e);
            }
        }
        else{
            return Writter::jsonError($response, "Missing data", 403);
        }

    }


    /** Méthode save
     * Edit a part of a photo
     * Boby : lat (optionnal),lng (optionnal), series (optionnal)
     * 
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
      */
      public function save(Request $request, Response $response, array $args): Response{
          $id = filter_var($args['id'], FILTER_SANITIZE_NUMBER_INT);

          try{
              $photo = Photo::find($id);

              if(!empty($photo)){
                $body = $request->getParsedBody();

                if(isset($body['lat'])){
                    $lat = filter_var($body['lat']);
                    $photo->lat = $lat;
                }

                if(isset($body['lng'])){
                    $lng = filter_var($body['lng']);
                    $photo->lng = $lng;
                }

                try{

                    if(isset($body['series'])){
                        $idSeries = filter_var($body['series']);
                        if($idSeries){
                            $series = Series::find($idSeries);
                            $photo->series()->associate($series);
                        }
                        else{
                            $photo->series()->dissociate();
                        }
                    }

                    $photo->save();

                    return Writter::jsonSuccess($response, array('success' => 1), 204);
                }
                catch(\Exception $e){
                    return PhpError::error($request, $response, $e);
                }

              }
              else{
                return NotFound::error($request, $response);
              }
          }
          catch(\Exception $e){
            return PhpError::error($request, $response, $e);
          }
      }

}