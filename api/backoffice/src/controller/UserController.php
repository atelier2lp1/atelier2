<?php

namespace geoQuizz\backoffice\controller;

use Firebase\JWT\JWT;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use geoQuizz\backoffice\response\Writter;
use geoQuizz\backoffice\mf\auth\Authentification;
use Ramsey\Uuid\Uuid;
/* Errors */
use geoQuizz\backoffice\errors\NotAllowed;
use geoQuizz\backoffice\errors\NotFound;
use geoQuizz\backoffice\errors\PhpError;
/* Models */
use geoQuizz\backoffice\models\User;

class UserController{

    protected $app;

    public function __construct($pApp){
        $this->app = $pApp;
    }

    /** Méthode login
     * Log a user with Auth Basic
     * 
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response (JWT token)
      */
    public function login(Request $request, Response $response, array $args): Response{
        if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])) {
            return Writter::jsonError($response, 'Credentials required', 401);
        }

        $email = $_SERVER['PHP_AUTH_USER'];
        $pass = $_SERVER['PHP_AUTH_PW'];

        try {
            $user = User::where('email', '=', $email)->first();
        } catch (\Exception $exception) {
            $user = false;
        }
        if ($user && password_verify($pass, $user->password)) {
            do {
                $key = random_bytes(32);
                $key = bin2hex($key);
                $keyUser = User::where('token','=',$key)->first(); 
            } while (!empty($keyUser));
            $user->token = $key;
            $user->save();

            $token = [
                "iss" => "http://api.backoffice.local",
                "aud" => "http://api.backoffice.local", //audience
                "uid" => $user->id,
                "iat" => time(),
                'exp' => time() + 36000,
            ];
            /*TODO sauvegarder la key dans un config file*/
            $jwt = JWT::encode($token, $key, 'HS512');

            $data = [
                "token" => $jwt
            ];
            return Writter::jsonSuccess($response, $data, 200);
            /*https://tools.ietf.org/html/rfc7519*/
        } else {
            return Writter::jsonError($response, 'Bad Credentials', 401);
        }
    }

    /** Méthode logout
     * logout the current user (user in the bearer token)
     * 
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
      */
    public function logout(Request $request, Response $response, array $args): Response{
        $token = Authentification::getToken($request);
        $user = User::find($token[1]->uid);
        try{
            $user->token= null;
            $user->save();

            return Writter::jsonSuccess($response, null, 204);
        }
        catch(\Exception $e){
            return PhpError::error($request, $response, $e);
        }
    }

    /** Méthode register
     * register add a new User
     * Body : email, password
     * 
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
      */
    public function register(Request $request, Response $response, array $args): Response{
        $body = $request->getParsedBody();
        $error = false;

        if(isset($body['email'])){
            $email = filter_var($body['email'], FILTER_SANITIZE_EMAIL);
            $user = User::where('email','=',$email)->first();

            if(!empty($user)){
                return Writter::jsonError($response, 'email already exist', 400);
            }
        }
        else{
            $error = true;
        }

        if(isset($body['password'])){
            $password = filter_var($body['password'], FILTER_SANITIZE_STRING);
        }
        else{
            $error = true;
        }

        if(!$error){
            try{
                do {
                    $uuid = Uuid::uuid4()->toString();
                    $uuidUser = User::find($uuid);
                } while (!empty($uuidUser));

                $newUser = new User();
                $newUser->id = $uuid;
                $newUser->email = $email;
                $newUser->password = Authentification::hashPassword($password);
                $newUser->save();

                return Writter::jsonSuccess($response, null, 204);
            }
            catch(\Exception $e){
                return PhpError::error($request, $response, $e);
            }
        }
        else{
            return Writter::jsonError($response, 'Missing data', 403);
        }
    }

}