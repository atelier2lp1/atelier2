<?php

namespace geoQuizz\backoffice\controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use geoQuizz\backoffice\response\Writter;
/* Errors */
use geoQuizz\backoffice\errors\NotAllowed;
use geoQuizz\backoffice\errors\NotFound;
use geoQuizz\backoffice\errors\PhpError;
/* Models */
use geoQuizz\backoffice\models\Level;

class LevelController{

    protected $app;

    public function __construct($pApp){
        $this->app = $pApp;
    }

    /** Méthode get
     * Get a part of level (pagination)
     * Params : page (optionnal)
     * 
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
      */
    public function get(Request $request, Response $response, array $args){
        $limit = 5;
        if(!empty($request->getQueryParam('page'))){
            $page = $request->getQueryParam('page');
        }
        else{
            $page = 1;
        }
        $offset = (--$page) * $limit;
        $count = Level::all()->count();
        $lastPage = ceil($count / $limit);
        $listLevels = Level::skip($offset)->take($limit)->get();

        $next = $page + 2;
        if($next > $lastPage){
            $next = $lastPage;
        }

        $prev = $page;
        if($prev == 0){
           $prev = 1; 
        }

        $data = [
            'count' => count($listLevels->toArray()),
            'levels' => $listLevels,
            'page' => [
                'prev' => $prev,
                'next' => $next,
                'first' => 1,
                'last' => $lastPage
            ]
        ];

        return Writter::jsonSuccess($response, $data, 206, 'collection');
    }


    /** Méthode create
     * Create a new level
     * Body : name, dist, nbphotos
     * 
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
      */
    public function create(Request $request, Response $response, array $args): Response{
        $body = $request->getParsedBody();
        $error = false;

        if(isset($body['name'])){
            $name = filter_var($body['name'], FILTER_SANITIZE_STRING);
        }
        else{
            $error = true;
        }

        if(isset($body['dist'])){
            $dist = filter_var($body['dist']);
        }
        else{
            $error = true;
        }

        if(isset($body['nbphotos'])){
            $nbphotos = filter_var($body['nbphotos'], FILTER_SANITIZE_NUMBER_INT);
        }
        else{
            $error = true;
        }

        if(!$error){
            try{
                $level = new Level();
                $level->name = $name;
                $level->dist = $dist;
                $level->nbphotos = $nbphotos;
                $level->save();

                return Writter::jsonSuccess($response, array('level' => $level), 201);
            }
            catch(\Exception $e){
                return PhpError::error($request, $response, $e);
            }
        }
        else{
            return Writter::jsonError($response, "Missing data", 403);
        }

    }

}