<?php
/**
 * Created by PhpStorm.
 * User: jalil
 * Date: 12/13/18
 * Time: 10:18 AM
 */
namespace geoQuizz\mobile\eloquent;

class Eloquent{

    public static function startEloquent($config){
        $db = new \Illuminate\Database\Capsule\Manager();

        $db->addConnection( parse_ini_file($config ));
        $db->setAsGlobal();
        $db->bootEloquent();


    }
}
