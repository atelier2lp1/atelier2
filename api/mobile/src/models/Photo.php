<?php

namespace geoQuizz\mobile\models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{

    public $timestamps = false;
    protected $table = 'photo';
    protected $primaryKey = 'id';
    protected $fillable = ['description', 'lat', 'lng', 'url', 'idSeries'];

    public function series()
    {
        return $this->belongsTo(
            'geoQuizz\mobile\models\Series',
            'idSeries');
    }

}