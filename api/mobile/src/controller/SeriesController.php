<?php
/**
 * Created by PhpStorm.
 * User: rubenconde
 * Date: 3/11/19
 * Time: 3:43 PM
 */

namespace geoQuizz\mobile\controller;


use geoQuizz\mobile\errors\NotFound;
use geoQuizz\mobile\models\Series;
use geoQuizz\mobile\response\Writter;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Container;

class SeriesController
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /** Méthode POST Photo
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
     */
    public function create(Request $req, Response $resp, array $args): Response
    {
        try {
            $params = $req->getParsedBody();

            $city = filter_var($params['city'], FILTER_SANITIZE_STRING);
            $lat = filter_var($params['lat'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $lng = filter_var($params['lng'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $zoom = filter_var($params['zoom'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

            $newSeries = Series::create(['city' => $city, 'lat' => $lat, 'lng' => $lng, 'zoom' => $zoom]);

            $data = ['message'=>'New series added successfully','series' => $newSeries];

            return Writter::jsonSuccess($resp, $data, 201);
        } catch (\Exception $exception) {
            return Writter::jsonError($resp, $exception->getMessage(), 404);
        }
    }

    public function get(Request $req, Response $resp, array $args): Response
    {
        try {
            $series = Series::all();
            $data = ['series' => $series];

            return Writter::jsonSuccess($resp, $data, 201, 'collection');
        } catch (\Exception $exception) {
            return NotFound::error($req, $resp);
        }
    }
}
