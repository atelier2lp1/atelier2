<?php
/**
 * Created by PhpStorm.
 * User: rubenconde
 * Date: 3/11/19
 * Time: 3:43 PM
 */

namespace geoQuizz\mobile\controller;


use geoQuizz\mobile\errors\NotFound;
use geoQuizz\mobile\models\Photo;
use geoQuizz\mobile\response\Writter;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Container;

class PhotoController
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /** Méthode POST Photo
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
     */
    public function create(Request $req, Response $resp, array $args): Response
    {
        try {
            $params = $req->getParsedBody();

            $description = filter_var($params['description'], FILTER_SANITIZE_STRING);
            $lat = filter_var($params['lat'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $lng = filter_var($params['lng'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $url = filter_var($params['url'], FILTER_SANITIZE_URL);
            if(isset($params['idSeries'])){
                $idSeries = filter_var($params['idSeries'], FILTER_SANITIZE_NUMBER_FLOAT);
            }else{
                $idSeries = null;
            }


            $newPhoto = Photo::create(['description' => $description, 'lat' => $lat, 'lng' => $lng, 'url' => $url, 'idSeries' => $idSeries]);

            $data = ['message'=>'New photo added successfully',
                'photo' => $newPhoto];

            return Writter::jsonSuccess($resp, $data, 201);

        } catch (\Exception $exception) {
            return Writter::jsonError($resp, $exception->getMessage(), 404);
        }
    }

    public function get(Request $req, Response $resp, array $args): Response
    {
        try {
            $photos = Photo::all();
            $data = ['photos' => $photos];

            return Writter::jsonSuccess($resp, $data, 201, 'collection');

        } catch (\Exception $exception) {
            return NotFound::error($req, $resp);
        }

    }
}