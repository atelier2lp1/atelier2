###API PLAYER http://api.player.local:10080

####Description :
L'API du joueur permet de gérer les jeux (ajouter, modifier, afficher), ainsi que les photos de chaque jeu en fonction de leur difficulté et de leur ville. L'utilisateur peut jouer grâce à une carte et des points avec les coordonnées GPS en fonction des points de l'image

| Route         	    | Method     | Header Params  			    | Body Params 									    | Description 				                 |
| ----------------------|:----------:| ----------------------------:|--------------------------------------------------:|-------------------------------------------:|
| /photos[/]            | GET        | none                         | none    										    | Affiche toutes les photos                  |
| /series[/]    	    | GET        | none                         | none    										    | Affiche toutes les series                  |
| /photos[/]    	    | POST 	     | none                         | description,lat,lng,url,idSeries(optionnel)       | Crée une photo avec ou sans serie          |
| /series[/]            | POST       | none                         | city,let,lng,zoom  							    | Crée une nouvelle serie                    |
